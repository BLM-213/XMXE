;;; init.el --- XMXE Initialization File


(defconst reimuxmx-emacs-min-version "25.0")
(when (version< emacs-version reimuxmx-emacs-min-version)
  (error "XMXE requires GNU Emacs %s or later" reimuxmx-emacs-min-version))

;;; Bootstrap configuration
;; (package-initialize)

(add-to-list 'load-path (expand-file-name "lisp" user-emacs-directory))

;; Measure startup time
(require 'mx-benchmarking)

(defconst on-macOS (eq system-type 'darwin))

;; Temporarily reduce garbage collection during startup
(defconst reimuxmx-initial-gc-cons-threshold gc-cons-threshold
  "Initial value of `gc-cons-threshold' at start-up time.")
(setq gc-cons-threshold 100000000)
(add-hook 'after-init-hook
          (lambda () (setq gc-cons-threshold reimuxmx-initial-gc-cons-threshold)))

(setq custom-file (expand-file-name "mxcus.el" user-emacs-directory))

(require 'cl-lib)

(require 'mx-utils)
(require 'mx-site-lisp)
(require 'mx-elpa)
(require 'mx-exec-path)

;; Allow users to provide an optional "mx-pre-private.el"
(require 'mx-pre-private nil t)

;;; Load all specific configurations
(require 'mx-base-packages)

;; Core
(require 'mx-frame-hooks)
(require 'mx-xterm)
(require 'mx-themes)
(require 'mx-macOS)
(require 'mx-ui)
(require 'mx-dired)
(require 'mx-isearch)
(require 'mx-grep)

;; Tools
(require 'mx-editing)
(require 'mx-ibuffer)
(require 'mx-flycheck)
(require 'mx-smex)
(require 'mx-ivy)
(require 'mx-completion)
(require 'mx-windows)
(require 'mx-desktop)
(require 'mx-fonts)
(require 'mx-mmm)

;; Input method
(require 'mx-pyim)

;; Version contral and project management
(require 'mx-vc)
(require 'mx-darcs)
(require 'mx-git)
(require 'mx-github)
(require 'mx-projectile)

;; Compile configuration
(require 'mx-compile)

;; Programming language support
(require 'mx-c-cpp)
(require 'mx-cmake)
(require 'mx-csv)
(require 'mx-erlang)
(require 'mx-golang)
(require 'mx-javascript)
(require 'mx-php)
(require 'mx-nxml)
(require 'mx-html)
(require 'mx-css)
(require 'mx-toml)
(require 'mx-yaml)
(require 'mx-markdown)
(require 'mx-org-mode)
(require 'mx-textile)
(require 'mx-python)
(require 'mx-haskell)
(require 'mx-elm)
(require 'mx-ruby)
(require 'mx-rails)
(require 'mx-sql)

;; Lisp
(require 'mx-paredit)
(require 'mx-lisp)
(require 'mx-slime)
(require 'mx-clojure)
(require 'mx-common-lisp)

;; Spell check
;; (require 'mx-spelling)

;; Miscellaneous
(require 'mx-misc)
(require 'mx-dash)
(require 'mx-folding)
(require 'mx-ledger)

;; Allow access from emacsclient
(require 'server)
(unless (server-running-p)
  (server-start))

;; Variables configured via the interactive 'customize' interface
(when (file-exists-p custom-file)
  (load custom-file))

;; Allow users to provide an optional "mx-private.el" containing private settings
(require 'mx-private nil t)

;; Locales (configure them earlier in this file doesn't work in X)
(require 'mx-locales)

;; Local Variables:
;; coding: utf-8
;; no-byte-compile: t
;; End:
