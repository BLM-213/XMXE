;;; mx-base-packages.el --- Base Packages Installation


(require-package 'wgrep)
(require-package 'project-local-variables)
(require-package 'diminish)
(require-package 'scratch)

(maybe-require-package 'dockerfile-mode)
(maybe-require-package 'lua-mode)
(maybe-require-package 'command-log-mode)
(maybe-require-package 'gnuplot)
(maybe-require-package 'htmlize)
(maybe-require-package 'regex-tool)

(provide 'mx-base-packages)
