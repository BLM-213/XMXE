;;; mx-benchmarking.el --- Startup Time Measurement Support


(defun reimuxmx/time-subtract-millis (b a)
  (* 1000.0 (float-time (time-subtract b a))))

(defvar reimuxmx-require-times nil
  "A list of (FEATURE . LOAD-DURATION).
LOAD-DURATION is the time taken in milliseconds to load FEATURE.")

(defadvice require (around reimuxmx/build-require-times (feature &optional filename noerror) activate)
  "Note in `reimuxmx-require-times' the time taken to require each feature."
  (let* ((already-loaded (memq feature features))
         (require-start-time (and (not already-loaded) (current-time))))
    (prog1
        ad-do-it
      (when (and (not already-loaded) (memq feature features))
        (let ((time (reimuxmx/time-subtract-millis (current-time) require-start-time)))
          (add-to-list 'reimuxmx-require-times
                       (cons feature time)
                       t))))))

(defun reimuxmx/show-init-time ()
  (message "Init completed in %.2fms"
           (reimuxmx/time-subtract-millis after-init-time before-init-time)))

(add-hook 'after-init-hook 'reimuxmx/show-init-time)

(provide 'mx-benchmarking)
