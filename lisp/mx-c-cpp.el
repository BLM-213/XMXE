;;; mx-c-cpp.el --- C / C++ Support


(when (maybe-require-package 'irony)
  (add-hook 'c-mode-hook 'irony-mode)
  (add-hook 'c++-mode-hook 'irony-mode)
  (add-hook 'irony-mode-hook 'irony-cdb-autosetup-compile-options)

  (with-eval-after-load 'irony
    (diminish 'irony-mode))

  (when (maybe-require-package 'irony-eldoc)
    (add-hook 'irony-mode-hook #'irony-eldoc)))

;; C headers completion
(when (maybe-require-package 'company-c-headers)
  (with-eval-after-load 'company
    (add-hook 'c-mode-common-hook
              (lambda () (reimuxmx/push-local-company-backend 'company-c-headers)))))

(provide 'mx-c-cpp)
