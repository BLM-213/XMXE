;;; mx-clojure.el --- Clojure Support


(when (maybe-require-package 'clojure-mode)
  (require-package 'cljsbuild-mode)
  (require-package 'elein)

  (with-eval-after-load 'clojure-mode
    (add-hook 'clojure-mode-hook 'reimuxmx/lisp-setup)
    (add-hook 'clojure-mode-hook 'subword-mode)))

;;; CIDER
(when (maybe-require-package 'cider)
  (setq nrepl-popup-stacktraces nil)

  (with-eval-after-load 'cider
    (add-hook 'cider-mode-hook 'eldoc-mode)
    (add-hook 'cider-repl-mode-hook 'subword-mode)
    (add-hook 'cider-repl-mode-hook 'paredit-mode)
    ;; nrepl isn't based on comint
    (add-hook 'cider-repl-mode-hook 'reimuxmx/no-trailing-whitespace))

  (require-package 'flycheck-clojure)
  (with-eval-after-load 'clojure-mode
    (with-eval-after-load 'cider
      (with-eval-after-load 'flycheck
        (flycheck-clojure-setup)))))

(provide 'mx-clojure)
