;;; mx-cmake.el --- CMake File Editing Support


(when (maybe-require-package 'cmake-mode)
  (reimuxmx/add-auto-mode 'cmake-mode "CMakeLists\\.txt\\'" "\\.cmake\\'"))

(provide 'mx-cmake)
