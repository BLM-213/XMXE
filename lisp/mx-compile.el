;;; mx-compile.el --- Compile Configuration


(setq-default compilation-scroll-output t)

(require-package 'alert)

;; Hint: Customize `alert-default-style' to get messages after compilation

(defun reimuxmx/alert-after-compilation-finish (buf result)
  "If BUF is hidden, use `alert' to report compilation RESULT."
  (when (buffer-live-p buf)
    (unless (catch 'is-visible
              (walk-windows (lambda (w)
                              (when (eq (window-buffer w) buf)
                                (throw 'is-visible t))))
              nil)
      (alert (concat "Compilation " result)
             :buffer buf
             :category 'compilation))))

(with-eval-after-load 'compile
  (add-hook 'compilation-finish-functions
            'reimuxmx/alert-after-compilation-finish))

(defvar reimuxmx-last-compilation-buffer nil
  "The last buffer in which compilation took place.")

(with-eval-after-load 'compile
  (defadvice compilation-start (after reimuxmx/save-compilation-buffer activate)
    "Save the compilation buffer to find it later."
    (setq reimuxmx-last-compilation-buffer next-error-last-buffer))

  (defadvice recompile (around reimuxmx/find-prev-compilation (&optional edit-command) activate)
    "Find the previous compilation buffer, if present, and recompile there."
    (if (and (null edit-command)
             (not (derived-mode-p 'compilation-mode))
             reimuxmx-last-compilation-buffer
             (buffer-live-p (get-buffer reimuxmx-last-compilation-buffer)))
        (with-current-buffer reimuxmx-last-compilation-buffer
          ad-do-it)
      ad-do-it)))

(global-set-key [f6] 'recompile)

(defadvice shell-command-on-region
    (after reimuxmx/shell-command-in-view-mode
           (start end command &optional output-buffer &rest other-args)
           activate)
  "Put \"*Shell Command Output*\" buffers into view-mode."
  (unless output-buffer
    (with-current-buffer "*Shell Command Output*"
      (view-mode 1))))

(with-eval-after-load 'compile
  (require 'ansi-color)
  (defun reimuxmx/colourise-compilation-buffer ()
    (when (eq major-mode 'compilation-mode)
      (ansi-color-apply-on-region compilation-filter-start (point-max))))
  (add-hook 'compilation-filter-hook 'reimuxmx/colourise-compilation-buffer))

(maybe-require-package 'cmd-to-echo)

(provide 'mx-compile)
