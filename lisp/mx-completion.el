;;; mx-completion.el --- Completion Configuration


(setq tab-always-indent 'complete)
(add-to-list 'completion-styles 'initials t)

;;; Hippie-expand
(setq hippie-expand-try-functions-list
      '(try-complete-file-name-partially
        try-complete-file-name
        try-expand-dabbrev
        try-expand-dabbrev-all-buffers
        try-expand-dabbrev-from-kill))

(global-set-key (kbd "M-/") 'hippie-expand)

;;; Company
(when (maybe-require-package 'company)
  (add-hook 'after-init-hook 'global-company-mode)
  (with-eval-after-load 'company
    (diminish 'company-mode "CA"))

  (when (maybe-require-package 'company-quickhelp)
    (company-quickhelp-mode 1)))

;; Handier way to add backend to a buffer-local version of company-backends
(defun reimuxmx/push-local-company-backend (backend)
  "Push BACKEND to a buffer-local version of `company-backends'."
  (make-local-variable 'company-backends)
  (push backend company-backends))

(provide 'mx-completion)
