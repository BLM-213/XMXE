;;; mx-csv.el --- CSV Support


(require-package 'csv-mode)
(require-package 'csv-nav)

(add-to-list 'auto-mode-alist '("\\.[Cc][Ss][Vv]\\'" . csv-mode))
(setq csv-separators '("," ";" "|" " "))

(provide 'mx-csv)
