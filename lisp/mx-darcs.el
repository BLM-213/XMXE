;;; mx-darcs.el --- Darcs Support


(require-package 'darcsum)
(require-package 'vc-darcs)

(add-to-list 'vc-handled-backends 'DARCS)
(autoload 'vc-darcs-find-file-hook "vc-darcs")
(add-hook 'find-file-hooks 'vc-darcs-find-file-hook)

(setq darcsum-whatsnew-switches "-l")

(provide 'mx-darcs)
