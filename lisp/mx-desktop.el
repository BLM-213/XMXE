;;; mx-desktop.el --- Save and Restore Desktop


(desktop-save-mode 1)

;; Save a bunch of variables to the desktop-file
;; for lists specify the length of the maximal saved data also
(setq desktop-globals-to-save
      (append '((comint-input-ring        . 64)
                (compile-history          . 32)
                desktop-missing-file-warning
                (dired-regexp-history     . 32)
                (extended-command-history . 32)
                (face-name-history        . 32)
                (file-name-history        . 128)
                (grep-find-history        . 32)
                (grep-history             . 32)
                (ido-buffer-history       . 128)
                (ido-last-directory-list  . 128)
                (ido-work-directory-list  . 128)
                (ido-work-file-list       . 128)
                (ivy-history              . 128)
                (magit-read-rev-history   . 64)
                (minibuffer-history       . 64)
                (org-clock-history        . 64)
                (org-refile-history       . 64)
                (org-tags-history         . 64)
                (query-replace-history    . 64)
                (read-expression-history  . 64)
                (regexp-history           . 64)
                (regexp-search-ring       . 32)
                register-alist
                (search-ring              . 32)
                (shell-command-history    . 64)
                tags-file-name
                tags-table-list)))

(provide 'mx-desktop)
