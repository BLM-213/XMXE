;;; mx-dired.el --- Dired Configuration


(require-package 'dired-sort)
(require-package 'diredfl)

;; Prefer g-prefixed coreutils version of standard utilities when available
(let ((gls (executable-find "gls")))
  (when gls (setq insert-directory-program gls)))

(with-eval-after-load 'dired
  (require 'dired-sort)
  (diredfl-global-mode)
  (setq-default dired-dwim-target t))

(provide 'mx-dired)
