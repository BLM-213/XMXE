;;; mx-elm.el --- Elm Support


(when (maybe-require-package 'elm-mode)
  (with-eval-after-load 'elm-mode
    (diminish 'elm-indent-mode)
    (with-eval-after-load 'company
      (add-hook 'elm-mode-hook
                (lambda () (reimuxmx/push-local-company-backend 'company-elm))))
    (when (executable-find "elm-format")
      (setq-default elm-format-on-save t)))
  (when (maybe-require-package 'flycheck-elm)
    (with-eval-after-load 'elm-mode
      (flycheck-elm-setup))))

(provide 'mx-elm)
