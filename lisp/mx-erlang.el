;;; mx-erlang.el --- Erlang Support


(ignore-errors
  (require-package 'erlang))

(when (package-installed-p 'erlang)
  (require 'erlang-start))

(provide 'mx-erlang)
