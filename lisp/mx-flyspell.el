;;; mx-flyspell.el --- Flyspell Configuration


(add-hook 'prog-mode-hook 'flyspell-prog-mode)
(with-eval-after-load 'flyspell
  (add-to-list 'flyspell-prog-text-faces 'nxml-text-face))

(provide 'mx-flyspell)
