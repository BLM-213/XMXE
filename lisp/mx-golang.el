;;; mx-golang.el --- Go Programming Language Support


(when (maybe-require-package 'go-mode)
  (add-hook 'go-mode-hook (lambda () (setq-local tab-width 4)))

  (when (maybe-require-package 'go-eldoc)
    (add-hook 'go-mode-hook 'go-eldoc-setup))

  ;; Company-go configurations
  (when (maybe-require-package 'company-go)
    (with-eval-after-load 'company
      (add-hook 'go-mode-hook
                (lambda () (reimuxmx/push-local-company-backend 'company-go))))))

(provide 'mx-golang)
