;;; mx-grep.el --- Grep Support


(setq-default grep-highlight-matches t
              grep-scroll-output t)

(when on-macOS
  (setq-default locate-command "mdfind"))

(when (executable-find "ag")
  (require-package 'ag)
  (require-package 'wgrep-ag)
  (setq-default ag-highlight-search t))

(provide 'mx-grep)
