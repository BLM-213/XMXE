;;; mx-haskell.el --- Haskell Support


(require-package 'haskell-mode)

;; Use intero for completion and flycheck
(when (maybe-require-package 'intero)
  (with-eval-after-load 'haskell-mode
    (intero-global-mode)
    (add-hook 'haskell-mode-hook 'eldoc-mode))
  (with-eval-after-load 'haskell-cabal
    (define-key haskell-cabal-mode-map (kbd "C-c C-l") 'intero-restart))
  (with-eval-after-load 'intero
    (with-eval-after-load 'flycheck
      (flycheck-add-next-checker 'intero
                                 '(warning . haskell-hlint)))))

(add-to-list 'auto-mode-alist '("\\.ghci\\'" . haskell-mode))

(add-hook 'haskell-mode-hook 'turn-on-haskell-indentation)

;;; Source code helpers
(add-hook 'haskell-mode-hook 'haskell-auto-insert-module-template)

(when (maybe-require-package 'hindent)
  (add-hook 'haskell-mode-hook 'hindent-mode))

(with-eval-after-load 'haskell-mode
  (define-key haskell-mode-map (kbd "C-c h") 'hoogle)
  (define-key haskell-mode-map (kbd "C-o") 'open-line))

(with-eval-after-load 'page-break-lines
  (push 'haskell-mode page-break-lines-modes))

(when (maybe-require-package 'dhall-mode)
  (add-hook 'dhall-mode-hook 'reimuxmx/no-trailing-whitespace))

(provide 'mx-haskell)
