;;; mx-html.el --- HTML Support


(require-package 'tidy)
(add-hook 'html-mode-hook (lambda () (tidy-build-menu html-mode-map)))

(require-package 'tagedit)
(with-eval-after-load 'sgml-mode
  (tagedit-add-paredit-like-keybindings)
  (add-hook 'sgml-mode-hook (lambda () (tagedit-mode 1))))

(add-to-list 'auto-mode-alist '("\\.\\(jsp\\|tmpl\\)\\'" . html-mode))

(require-package 'haml-mode)

(provide 'mx-html)
