;;; mx-ivy.el --- Ivy Configuration


(when (maybe-require-package 'ivy)
  (ivy-mode 1)
  (setq-default ivy-use-virtual-buffers t
                projectile-completion-system 'ivy
                ivy-initial-inputs-alist
                '((man . "^")
                  (woman . "^")))

  (with-eval-after-load 'ivy
    (diminish 'ivy-mode)))

(when (maybe-require-package 'ivy-xref)
  (setq xref-show-xrefs-function 'ivy-xref-show-xrefs))

(when (maybe-require-package 'ivy-historian)
  (ivy-historian-mode t))

(when (maybe-require-package 'counsel)
  (counsel-mode 1)
  (setq-default counsel-mode-override-describe-bindings t)

  (with-eval-after-load 'counsel
    (diminish 'counsel-mode))

  (when (and (executable-find "ag")
             (maybe-require-package 'projectile))
    (defun reimuxmx/counsel-ag-project (initial-input &optional use-current-dir)
      "Search using `counsel-ag' from the project root for INITIAL-INPUT.
If there is no project root, or if the prefix argument
USE-CURRENT-DIR is set, then search from the current directory
instead."
      (interactive (list (thing-at-point 'symbol)
                         current-prefix-arg))
      (let ((current-prefix-arg)
            (dir (if use-current-dir
                     default-directory
                   (condition-case err
                       (projectile-project-root)
                     (error default-directory)))))
        (counsel-ag initial-input dir)))

    (global-set-key (kbd "M-?") 'reimuxmx/counsel-ag-project)))

(when (maybe-require-package 'swiper)
  (with-eval-after-load 'ivy
    (defun reimuxmx/swiper-at-point (sym)
      "Use `swiper' to search for the symbol at point."
      (interactive (list (thing-at-point 'symbol)))
      (swiper sym))

    (define-key ivy-mode-map (kbd "M-s /") 'reimuxmx/swiper-at-point)))

(provide 'mx-ivy)
