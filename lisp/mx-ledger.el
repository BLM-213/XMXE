;;; mx-ledger.el --- Ledger Support


(when (maybe-require-package 'ledger-mode)

  (add-to-list 'auto-mode-alist '("\\.ledger$" . ledger-mode))

  (when (maybe-require-package 'flycheck-ledger)
    (with-eval-after-load 'flycheck
      (with-eval-after-load 'ledger-mode
        (require 'flycheck-ledger))))

  (with-eval-after-load 'ledger-mode
    (define-key ledger-mode-map (kbd "RET") 'newline))

  (setq ledger-highlight-xact-under-point nil
        ledger-use-iso-dates nil)

  (with-eval-after-load 'ledger-mode
    (when (memq window-system '(mac ns))
      (exec-path-from-shell-copy-env "LEDGER_FILE")))

  (add-hook 'ledger-mode-hook 'goto-address-prog-mode))

(provide 'mx-ledger)
