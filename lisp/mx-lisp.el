;;; mx-lisp.el --- Lisp Support


(require-package 'elisp-slime-nav)
(dolist (hook '(emacs-lisp-mode-hook ielm-mode-hook))
  (add-hook hook 'turn-on-elisp-slime-nav-mode))
(with-eval-after-load 'elisp-slime-nav
  (diminish 'elisp-slime-nav-mode))

;; Make C-x C-e run 'eval-region if the region is active
(defun reimuxmx/eval-last-sexp-or-region (prefix)
  "Eval region from BEG to END if active, otherwise the last sexp."
  (interactive "P")
  (if (and (mark) (use-region-p))
      (eval-region (min (point) (mark)) (max (point) (mark)))
    (pp-eval-last-sexp prefix)))

(global-set-key [remap eval-expression] 'pp-eval-expression)

(with-eval-after-load 'lisp-mode
  (define-key emacs-lisp-mode-map (kbd "C-x C-e") 'reimuxmx/eval-last-sexp-or-region))

(defadvice pp-display-expression (after reimuxmx/make-read-only (expression out-buffer-name) activate)
  "Enable `view-mode' in the output buffer - if any - so it can be closed with `\"q\"."
  (when (get-buffer out-buffer-name)
    (with-current-buffer out-buffer-name
      (view-mode 1))))

(defun reimuxmx/maybe-set-bundled-elisp-readonly ()
  "If this elisp appears to be part of Emacs, then disallow editing."
  (when (and (buffer-file-name)
             (string-match-p "\\.el\\.gz\\'" (buffer-file-name)))
    (setq buffer-read-only t)
    (view-mode 1)))

(add-hook 'emacs-lisp-mode-hook 'reimuxmx/maybe-set-bundled-elisp-readonly)

;; Use C-c C-z to toggle between elisp files and an ielm session
;; I might generalise this to ruby etc., or even just adopt the repl-toggle package.
(defvar reimuxmx-repl-original-buffer nil
  "Buffer from which we jumped to this REPL.")
(make-variable-buffer-local 'reimuxmx-repl-original-buffer)

(defvar reimuxmx-repl-switch-function 'switch-to-buffer-other-window)

(defun reimuxmx/switch-to-ielm ()
  (interactive)
  (let ((orig-buffer (current-buffer)))
    (if (get-buffer "*ielm*")
        (funcall reimuxmx-repl-switch-function "*ielm*")
      (ielm))
    (setq reimuxmx-repl-original-buffer orig-buffer)))

(defun reimuxmx/repl-switch-back ()
  "Switch back to the buffer from which we reached this REPL."
  (interactive)
  (if reimuxmx-repl-original-buffer
      (funcall reimuxmx-repl-switch-function reimuxmx-repl-original-buffer)
    (error "No original buffer")))

(with-eval-after-load 'elisp-mode
  (define-key emacs-lisp-mode-map (kbd "C-c C-z") 'reimuxmx/switch-to-ielm))
(with-eval-after-load 'ielm
  (define-key ielm-map (kbd "C-c C-z") 'reimuxmx/repl-switch-back))

;;; Hippie-expand
(defun reimuxmx/set-up-hippie-expand-for-elisp ()
  "Locally set `hippie-expand' completion functions for use with Emacs Lisp."
  (make-local-variable 'hippie-expand-try-functions-list)
  (add-to-list 'hippie-expand-try-functions-list 'try-complete-lisp-symbol t)
  (add-to-list 'hippie-expand-try-functions-list 'try-complete-lisp-symbol-partially t)
  (add-to-list 'hippie-expand-try-functions-list 'my/try-complete-lisp-symbol-without-namespace t))

;; Automatic byte compilation
(when (maybe-require-package 'auto-compile)
  (auto-compile-on-save-mode)
  (auto-compile-on-load-mode))

;; Load .el if newer than corresponding .elc
(setq load-prefer-newer t)

;; Support byte-compilation in a sub-process, as
;; required by highlight-cl
(defun reimuxmx/byte-compile-file-batch (filename)
  "Byte-compile FILENAME in batch mode, ie. a clean sub-process."
  (interactive "fFile to byte-compile in batch mode: ")
  (let ((emacs (car command-line-args)))
    (compile
     (concat
      emacs " "
      (mapconcat
       'shell-quote-argument
       (list "-Q" "-batch" "-f" "batch-byte-compile" filename)
       " ")))))

(defun reimuxmx/enable-check-parens-on-save ()
  "Run `check-parens' when the current buffer is saved."
  (add-hook 'after-save-hook #'check-parens nil t))

(defun reimuxmx/disable-indent-guide ()
  (when (bound-and-true-p indent-guide-mode)
    (indent-guide-mode -1)))

(defvar reimuxmx-lispy-modes-hook
  '(enable-paredit-mode
    turn-on-eldoc-mode
    reimuxmx/disable-indent-guide
    reimuxmx/enable-check-parens-on-save)
  "Hook run in all Lisp modes.")

(when (maybe-require-package 'aggressive-indent)
  (add-to-list 'reimuxmx-lispy-modes-hook 'aggressive-indent-mode))

(defun reimuxmx/lisp-setup ()
  "Enable features useful in any Lisp mode."
  (run-hooks 'reimuxmx-lispy-modes-hook))

(defun reimuxmx/emacs-lisp-setup ()
  "Enable features useful when working with elisp."
  (reimuxmx/set-up-hippie-expand-for-elisp))

(defconst reimuxmx-elispy-modes
  '(emacs-lisp-mode ielm-mode)
  "Major modes relating to elisp.")

(defconst reimuxmx-lispy-modes
  (append reimuxmx-elispy-modes
          '(lisp-mode inferior-lisp-mode lisp-interaction-mode))
  "All lispy major modes.")

(require 'derived)

(dolist (hook (mapcar #'derived-mode-hook-name reimuxmx-lispy-modes))
  (add-hook hook 'reimuxmx/lisp-setup))

(dolist (hook (mapcar #'derived-mode-hook-name reimuxmx-elispy-modes))
  (add-hook hook 'reimuxmx/emacs-lisp-setup))

(if (boundp 'eval-expression-minibuffer-setup-hook)
    (add-hook 'eval-expression-minibuffer-setup-hook #'eldoc-mode)
  (require-package 'eldoc-eval)
  (require 'eldoc-eval)
  (eldoc-in-minibuffer-mode 1))

(add-to-list 'auto-mode-alist '("\\.emacs-project\\'" . emacs-lisp-mode))
(add-to-list 'auto-mode-alist '("archive-contents\\'" . emacs-lisp-mode))

(require-package 'cl-lib-highlight)
(with-eval-after-load 'lisp-mode
  (cl-lib-highlight-initialize))

;; Delete .elc files when reverting the .el from VC or magit
;; When .el files are open, we can intercept when they are modified
;; by VC or magit in order to remove .elc files that are likely to
;; be out of sync.

;; This is handy while actively working on elisp files, though
;; obviously it doesn't ensure that unopened files will also have
;; their .elc counterparts removed - VC hooks would be necessary for
;; that.

(defvar reimuxmx-vc-reverting nil
  "Whether or not VC or Magit is currently reverting buffers.")

(defadvice revert-buffer (after reimuxmx/maybe-remove-elc activate)
  "If reverting from VC, delete any .elc file that will now be out of sync."
  (when reimuxmx-vc-reverting
    (when (and (eq 'emacs-lisp-mode major-mode)
               buffer-file-name
               (string= "el" (file-name-extension buffer-file-name)))
      (let ((elc (concat buffer-file-name "c")))
        (when (file-exists-p elc)
          (message "Removing out-of-sync elc file %s" (file-name-nondirectory elc))
          (delete-file elc))))))

(defadvice magit-revert-buffers (around reimuxmx/reverting activate)
  (let ((reimuxmx-vc-reverting t))
    ad-do-it))
(defadvice vc-revert-buffer-internal (around reimuxmx/reverting activate)
  (let ((reimuxmx-vc-reverting t))
    ad-do-it))

(require-package 'macrostep)

(with-eval-after-load 'lisp-mode
  (define-key emacs-lisp-mode-map (kbd "C-c e") 'macrostep-expand))

;; A quick way to jump to the definition of a function given its key binding
(global-set-key (kbd "C-h K") 'find-function-on-key)

;;; Extras for theme editing
(defvar reimuxmx-theme-mode-hook nil
  "Hook triggered when editing a theme file.")

(defun reimuxmx/run-theme-mode-hooks-if-theme ()
  "Run `reimuxmx-theme-mode-hook' if this appears to a theme."
  (when (string-match "\\(color-theme-\\|-theme\\.el\\)" (buffer-name))
    (run-hooks 'reimuxmx-theme-mode-hook)))

(add-hook 'emacs-lisp-mode-hook 'reimuxmx/run-theme-mode-hooks-if-theme t)

(when (maybe-require-package 'rainbow-mode)
  (add-hook 'reimuxmx-theme-mode-hook 'rainbow-mode))

(when (maybe-require-package 'aggressive-indent)
  ;; Can be prohibitively slow with very long forms
  (add-to-list 'reimuxmx-theme-mode-hook (lambda () (aggressive-indent-mode -1)) t))

(when (maybe-require-package 'highlight-quoted)
  (add-hook 'emacs-lisp-mode-hook 'highlight-quoted-mode))

(when (maybe-require-package 'flycheck)
  (require-package 'flycheck-package)
  (with-eval-after-load 'flycheck
    (flycheck-package-setup)))

;;; ERT
(with-eval-after-load 'ert
  (define-key ert-results-mode-map (kbd "g") 'ert-results-rerun-all-tests))

(defun reimuxmx/cl-libify-next ()
  "Find next symbol from 'cl and replace it with the 'cl-lib equivalent."
  (interactive)
  (let ((case-fold-search nil))
    (re-search-forward
     (concat
      "("
      (regexp-opt
       ;; Not an exhaustive list
       '("loop" "incf" "plusp" "first" "decf" "minusp" "assert"
         "case" "destructuring-bind" "second" "third" "defun*"
         "defmacro*" "return-from" "labels" "cadar" "fourth"
         "cadadr") t)
      "\\_>")))
  (let ((form (match-string 1)))
    (backward-sexp)
    (cond
     ((string-match "^\\(defun\\|defmacro\\)\\*$")
      (kill-sexp)
      (insert (concat "cl-" (match-string 1))))
     (t
      (insert "cl-")))
    (when (fboundp 'aggressive-indent-indent-defun)
      (aggressive-indent-indent-defun))))

(maybe-require-package 'cask-mode)

(provide 'mx-lisp)
