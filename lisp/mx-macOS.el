;;; mx-macOS.el --- macOS Configuration


(when on-macOS
  (require-package 'osx-location)
  (with-eval-after-load 'term
    (define-key term-raw-map (kbd "s-v") 'term-paste))
  (if (fboundp 'set-fontset-font)
      (set-fontset-font t 'unicode "Apple Color Emoji" nil 'prepend)))

(provide 'mx-macOS)
