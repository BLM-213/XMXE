;;; mx-misc.el --- Miscellaneous Configuration


(fset 'yes-or-no-p 'y-or-n-p)

(add-hook 'prog-mode-hook 'goto-address-prog-mode)
(setq goto-address-mail-face 'link)

(add-hook 'after-save-hook 'executable-make-buffer-file-executable-if-script-p)

(with-eval-after-load 'eldoc
  (diminish 'eldoc-mode))

(setq-default regex-tool-backend 'perl)

(add-to-list 'auto-mode-alist '("Portfile\\'" . tcl-mode))

(add-to-list 'auto-mode-alist '("Procfile" . conf-mode))

(provide 'mx-misc)
