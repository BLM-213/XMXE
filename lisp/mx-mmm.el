;;; mx-mmm.el --- Multiple Major Modes Support


(require-package 'mmm-mode)

(require 'mmm-auto)
(setq mmm-global-mode 'buffers-with-submode-classes)
(setq mmm-submode-decoration-level 2)

(provide 'mx-mmm)
