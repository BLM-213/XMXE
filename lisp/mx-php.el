;;; mx-php.el --- PHP Support


(when (maybe-require-package 'php-mode)
  (maybe-require-package 'smarty-mode))

(provide 'mx-php)
