;;; mx-projectile.el --- Projectile Support


(when (maybe-require-package 'projectile)
  (add-hook 'after-init-hook 'projectile-mode)

  ;; Shorter modeline
  (with-eval-after-load 'projectile
    (setq-default
     projectile-mode-line
     '(:eval
       (if (file-remote-p default-directory)
           " PO"
         (format " PO[%s]" (projectile-project-name)))))))

(provide 'mx-projectile)
