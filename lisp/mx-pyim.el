;;; mx-pyim.el --- Pyim Configuration


(require-package 'pyim)
(require-package 'pyim-basedict)
(require 'pyim)
(require 'pyim-basedict)
(with-eval-after-load 'pyim
  (pyim-basedict-enable))

;; Feature configurations
(setq pyim-default-scheme 'quanpin)
(setq pyim-page-tooltip 'popup
      pyim-page-length 4)

(provide 'mx-pyim)
