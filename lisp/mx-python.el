;;; mx-python.el --- Python Support


(reimuxmx/add-auto-mode 'python-mode "SConstruct\\'" "SConscript\\'")

(require-package 'pip-requirements)

(when (maybe-require-package 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-mode)
  (add-hook 'python-mode-hook 'anaconda-eldoc-mode)
  (when (maybe-require-package 'company-anaconda)
    (with-eval-after-load 'company
      (add-hook 'python-mode-hook
                (lambda () (reimuxmx/push-local-company-backend 'company-anaconda))))))

(provide 'mx-python)
