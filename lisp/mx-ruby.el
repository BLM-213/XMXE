;;; mx-ruby.el --- Ruby Support


;;; Basic ruby setup
(require-package 'ruby-mode)
(require-package 'ruby-hash-syntax)

(reimuxmx/add-auto-mode 'ruby-mode
                        "Rakefile\\'" "\\.rake\\'" "\\.rxml\\'"
                        "\\.rjs\\'" "\\.irbrc\\'" "\\.pryrc\\'" "\\.builder\\'" "\\.ru\\'"
                        "\\.gemspec\\'" "Gemfile\\'" "Kirkfile\\'")

(add-to-list 'auto-mode-alist '("Gemfile\\.lock\\'" . conf-mode))

(setq-default
 ruby-use-encoding-map nil
 ruby-insert-encoding-magic-comment nil)

(add-hook 'ruby-mode-hook 'subword-mode)

(with-eval-after-load 'page-break-lines
  (push 'ruby-mode page-break-lines-modes))

(require-package 'rspec-mode)

;;; Inferior ruby
(require-package 'inf-ruby)

;;; Ruby compilation
(require-package 'ruby-compilation)

(with-eval-after-load 'ruby-mode
  (let ((m ruby-mode-map))
    (define-key m [S-f7] 'ruby-compilation-this-buffer)
    (define-key m [f7] 'ruby-compilation-this-test)))

(with-eval-after-load 'ruby-compilation
  (defalias 'rake 'ruby-compilation-rake))

;;; Robe
(when (maybe-require-package 'robe)
  (add-hook 'ruby-mode-hook 'robe-mode)
  (with-eval-after-load 'company
    (dolist (hook '(ruby-mode-hook inf-ruby-mode-hook html-erb-mode-hook haml-mode-hook))
      (add-hook hook
                (lambda () (reimuxmx/push-local-company-backend 'company-robe))))))

;; ri support
(require-package 'yari)
(defalias 'ri 'yari)

(require-package 'goto-gem)

(require-package 'bundler)

;;; ERB
(require-package 'mmm-mode)
(defun reimuxmx/loaded-mmm-erb ()
  (require 'mmm-erb))

(require 'derived)

(defun reimuxmx/set-up-mode-for-erb (mode)
  (add-hook (derived-mode-hook-name mode) 'reimuxmx/loaded-mmm-erb)
  (mmm-add-mode-ext-class mode "\\.erb\\'" 'erb))

(let ((html-erb-modes '(html-mode html-erb-mode nxml-mode)))
  (dolist (mode html-erb-modes)
    (reimuxmx/set-up-mode-for-erb mode)
    (mmm-add-mode-ext-class mode "\\.r?html\\(\\.erb\\)?\\'" 'html-js)
    (mmm-add-mode-ext-class mode "\\.r?html\\(\\.erb\\)?\\'" 'html-css)))

(mapc 'reimuxmx/set-up-mode-for-erb
      '(coffee-mode js-mode js2-mode js3-mode markdown-mode textile-mode))

(mmm-add-mode-ext-class 'html-erb-mode "\\.jst\\.ejs\\'" 'ejs)

(reimuxmx/add-auto-mode 'html-erb-mode
                        "\\.rhtml\\'"
                        "\\.html\\.erb\\'"
                        "\\.jst\\.ejs\\'")

(mmm-add-mode-ext-class 'yaml-mode "\\.yaml\\(\\.erb\\)?\\'" 'erb)
(reimuxmx/set-up-mode-for-erb 'yaml-mode)

(dolist (mode (list 'js-mode 'js2-mode 'js3-mode))
  (mmm-add-mode-ext-class mode "\\.js\\.erb\\'" 'erb))

;; Ruby - my convention for heredocs containing SQL
;; Needs to run after rinari to avoid clobbering font-lock-keywords?

;; (require-package 'mmm-mode)
;; (eval-after-load 'mmm-mode
;;   '(progn
;;      (mmm-add-classes
;;       '((ruby-heredoc-sql
;;          :submode sql-mode
;;          :front "<<-?[\'\"]?\\(end_sql\\)[\'\"]?"
;;          :save-matches 1
;;          :front-offset (end-of-line 1)
;;          :back "^[ \t]*~1$"
;;          :delimiter-mode nil)))
;;      (mmm-add-mode-ext-class 'ruby-mode "\\.rb\\'" 'ruby-heredoc-sql)))
;; (add-to-list 'mmm-set-file-name-for-modes 'ruby-mode)

(provide 'mx-ruby)
