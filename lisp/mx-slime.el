;;; mx-slime.el --- SLIME Configuration


(require-package 'slime)
(require-package 'hippie-expand-slime)
(maybe-require-package 'slime-company)

;;; Lisp buffers
(defun reimuxmx/slime-setup ()
  "Mode setup function for SLIME Lisp buffers."
  (set-up-slime-hippie-expand))

(with-eval-after-load 'slime
  (setq slime-protocol-version 'ignore)
  (setq slime-net-coding-system 'utf-8-unix)
  (let ((extras (when (require 'slime-company nil t)
                  '(slime-company))))
    (slime-setup (append '(slime-repl slime-fuzzy) extras)))
  (setq slime-complete-symbol*-fancy t)
  (setq slime-complete-symbol-function 'slime-fuzzy-complete-symbol)
  (add-hook 'slime-mode-hook 'reimuxmx/slime-setup))

;;; REPL
(defun reimuxmx/slime-repl-setup ()
  "Mode setup function for SLIME REPL."
  (reimuxmx/lisp-setup)
  (set-up-slime-hippie-expand)
  (setq show-trailing-whitespace nil))

(with-eval-after-load 'slime-repl
  ;; Stop SLIME's REPL from grabbing DEL, which is annoying when backspacing over a '('
  (with-eval-after-load 'paredit
    (define-key slime-repl-mode-map (read-kbd-macro paredit-backward-delete-key) nil))

  ;; Bind TAB to `indent-for-tab-command', as in regular SLIME buffers.
  (define-key slime-repl-mode-map (kbd "TAB") 'indent-for-tab-command)

  (add-hook 'slime-repl-mode-hook 'reimuxmx/slime-repl-setup))

(provide 'mx-slime)
