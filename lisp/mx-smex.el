;;; mx-smex.el --- Smex Configuration


(when (maybe-require-package 'smex)
  (setq-default smex-history-length 16
                smex-save-file (expand-file-name ".smex-items" user-emacs-directory))

  (global-set-key [remap execute-extended-command] 'smex))

(provide 'mx-smex)
