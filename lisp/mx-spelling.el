;;; mx-spelling.el --- Spell Check Support


(require 'ispell)

;; Enable Flyspell if spell checker exists
(when (executable-find ispell-program-name)
  (require 'mx-flyspell))

(provide 'mx-spelling)
