;;; mx-sql.el --- SQL Support


(require-package 'sql-indent)
(with-eval-after-load 'sql
  (require 'sql-indent))

(with-eval-after-load 'sql
  ;; sql-mode pretty much requires your psql to be uncustomised from stock settings
  (push "--no-psqlrc" sql-postgres-options))

(defun reimuxmx/pop-to-sqli-buffer ()
  "Switch to the corresponding sqli buffer."
  (interactive)
  (if sql-buffer
      (progn
        (pop-to-buffer sql-buffer)
        (goto-char (point-max)))
    (sql-set-sqli-buffer)
    (when sql-buffer
      (reimuxmx/pop-to-sqli-buffer))))

(with-eval-after-load 'sql
  (define-key sql-mode-map (kbd "C-c C-z") 'reimuxmx/pop-to-sqli-buffer)
  (when (package-installed-p 'dash-at-point)
    (defun reimuxmx/maybe-set-dash-db-docset ()
      (when (eq sql-product 'postgres)
        (set (make-local-variable 'dash-at-point-docset) "psql")))

    (add-hook 'sql-mode-hook 'reimuxmx/maybe-set-dash-db-docset)
    (add-hook 'sql-interactive-mode-hook 'reimuxmx/maybe-set-dash-db-docset)
    (defadvice sql-set-product (after set-dash-docset activate)
      (reimuxmx/maybe-set-dash-db-docset))))

(setq-default sql-input-ring-file-name
              (expand-file-name ".sqli_history" user-emacs-directory))

;; See purcell's answer to https://emacs.stackexchange.com/questions/657/why-do-sql-mode-and-sql-interactive-mode-not-highlight-strings-the-same-way/673
(defun reimuxmx/font-lock-everything-in-sql-interactive-mode ()
  (unless (eq 'oracle sql-product)
    (sql-product-font-lock nil nil)))
(add-hook 'sql-interactive-mode-hook 'reimuxmx/font-lock-everything-in-sql-interactive-mode)

(with-eval-after-load 'page-break-lines
  (push 'sql-mode page-break-lines-modes))

(provide 'mx-sql)
