;;; mx-textile.el --- Textile Support


(require-package 'textile-mode)

(add-to-list 'auto-mode-alist '("\\.textile\\'" . textile-mode))

(provide 'mx-textile)
