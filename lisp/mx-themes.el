;;; mx-themes.el --- Theme Configuration


(require-package 'leuven-theme)
(require-package 'zenburn-theme)

(setq-default custom-enabled-themes '(leuven))

;; Ensure that themes will be applied even if they have not been customized
(defun reimuxmx/reapply-themes ()
  "Forcibly load the themes listed in `custom-enabled-themes'."
  (dolist (theme custom-enabled-themes)
    (unless (custom-theme-p theme)
      (load-theme theme)))
  (custom-set-variables `(custom-enabled-themes (quote ,custom-enabled-themes))))

(add-hook 'after-init-hook 'reimuxmx/reapply-themes)

;; Toggle between light and dark
(defun reimuxmx/light ()
  "Activate a light color theme."
  (interactive)
  (setq custom-enabled-themes '(leuven))
  (reimuxmx/reapply-themes))

(defun reimuxmx/dark ()
  "Activate a dark color theme."
  (interactive)
  (setq custom-enabled-themes '(zenburn))
  (reimuxmx/reapply-themes))

(provide 'mx-themes)
