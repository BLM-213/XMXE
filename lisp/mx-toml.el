;;; mx-toml.el --- TOML Support


(when (maybe-require-package 'toml-mode)
  (add-hook 'toml-mode-hook 'goto-address-prog-mode))

(provide 'mx-toml)
