;;; mx-ui.el --- UI Configuration


(setq-default initial-scratch-message
              (concat ";; Happy Hacking, " user-login-name " ∞ Emacs ♥ You!\n\n"))

(require-package 'immortal-scratch)
(add-hook 'after-init-hook 'immortal-scratch-mode)

;;; GUI features
(setq inhibit-startup-screen t)

;; Window features
(when (fboundp 'tool-bar-mode)
  (tool-bar-mode -1))
(when (fboundp 'set-scroll-bar-mode)
  (set-scroll-bar-mode nil))
(when (fboundp 'menu-bar-mode)
  (menu-bar-mode -1))

;; Disable frame border
(let ((no-border '(internal-border-width . 0)))
  (add-to-list 'default-frame-alist no-border)
  (add-to-list 'initial-frame-alist no-border))

;;; Transparency
(defun reimuxmx/set-transparency (value &optional frame)
  "Set transparency to VALUE for FRAME.
The VALUE is between 0-100, 0 is transparent and 100 is opaque.
If FRAME is nil, it defaults to the selected frame."
  (interactive "nTransparency (0 is transparent - 100 is opaque): ")
  (set-frame-parameter frame 'alpha value))

(defun reimuxmx/disable-transparency (&optional frame)
  "Disable transparency for FRAME.
If FRAME is nil, it defaults to the selected frame."
  (interactive)
  (set-frame-parameter frame 'alpha '(100 . 100)))

(provide 'mx-ui)
