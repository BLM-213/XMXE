;;; mx-windows.el --- Window Configuration    -*- lexical-binding: t -*-


;; Navigate window layouts with "C-c <left>" and "C-c <right>"
(add-hook 'after-init-hook 'winner-mode)

(require-package 'switch-window)

(setq-default switch-window-shortcut-style 'qwerty)
(setq-default switch-window-qwerty-shortcuts
              '("a" "s" "d" "f" "j" "k" "l" ";" "w" "e" "i" "o"))

(global-set-key (kbd "C-x o") 'switch-window)

;; When splitting window, show (other-buffer) in the new window
(defun split-window-func-with-other-buffer (split-function)
  (lambda (&optional arg)
    "Split this window and switch to the new window unless ARG is provided."
    (interactive "P")
    (funcall split-function)
    (let ((target-window (next-window)))
      (set-window-buffer target-window (other-buffer))
      (unless arg
        (select-window target-window)))))

(global-set-key (kbd "C-x 2") (split-window-func-with-other-buffer 'split-window-vertically))
(global-set-key (kbd "C-x 3") (split-window-func-with-other-buffer 'split-window-horizontally))

;; Replace the key-based for which-key
(with-eval-after-load 'which-key
  (which-key-add-key-based-replacements
    "C-x 2" "split-window-vertically"
    "C-x 3" "split-window-horizontally"))

(defun reimuxmx/toggle-delete-other-windows ()
  "Delete other windows in frame if any, or restore previous window config."
  (interactive)
  (if (and winner-mode
           (equal (selected-window) (next-window)))
      (winner-undo)
    (delete-other-windows)))

(global-set-key (kbd "C-x 1") 'reimuxmx/toggle-delete-other-windows)

;; Rearrange split windows
(defun split-window-horizontally-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-horizontally))))

(defun split-window-vertically-instead ()
  (interactive)
  (save-excursion
    (delete-other-windows)
    (funcall (split-window-func-with-other-buffer 'split-window-vertically))))

(global-set-key (kbd "C-x |") 'split-window-horizontally-instead)
(global-set-key (kbd "C-x _") 'split-window-vertically-instead)

;; Borrowed from http://postmomentum.ch/blog/201304/blog-on-emacs
(defun reimuxmx/split-window()
  "Split the window to see the most recent buffer in the other window.
Call a second time to restore the original window configuration."
  (interactive)
  (if (eq last-command 'reimuxmx/split-window)
      (progn
        (jump-to-register :reimuxmx/split-window)
        (setq this-command 'reimuxmx/unsplit-window))
    (window-configuration-to-register :reimuxmx/split-window)
    (switch-to-buffer-other-window nil)))

(defun reimuxmx/toggle-current-window-dedication ()
  "Toggle whether the current window is dedicated to its current buffer."
  (interactive)
  (let* ((window (selected-window))
         (was-dedicated (window-dedicated-p window)))
    (set-window-dedicated-p window (not was-dedicated))
    (message "Window %sdedicated to %s"
             (if was-dedicated "no longer " "")
             (buffer-name))))

(unless (memq window-system '(nt w32))
  (windmove-default-keybindings 'control))

(provide 'mx-windows)
